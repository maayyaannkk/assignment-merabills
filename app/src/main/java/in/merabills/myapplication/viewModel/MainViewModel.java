package in.merabills.myapplication.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import in.merabills.myapplication.data.PaymentDetail;
import in.merabills.myapplication.data.PaymentType;
import in.merabills.myapplication.util.Utility;

public class MainViewModel extends AndroidViewModel {
    private final ArrayList<PaymentType> paymentTypes;
    private ArrayList<PaymentType> paymentTypesIncluded;
    private ArrayList<PaymentDetail> paymentDetails;
    private final MutableLiveData<ArrayList<PaymentDetail>> paymentDetailsLiveData;

    public MainViewModel(@NonNull Application application) {
        super(application);
        paymentTypes = Utility.getPaymentTypes(application);
        paymentTypesIncluded = new ArrayList<>(paymentTypes);
        paymentDetailsLiveData = new MutableLiveData<>();
        setPaymentDetails(Utility.getPaymentDetails(application));
    }

    public ArrayList<PaymentType> getPaymentTypesIncluded() {
        return paymentTypesIncluded;
    }

    public MutableLiveData<ArrayList<PaymentDetail>> getPaymentDetailsLiveData() {
        return paymentDetailsLiveData;
    }

    public ArrayList<PaymentDetail> getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(ArrayList<PaymentDetail> paymentDetails) {
        this.paymentDetails = paymentDetails;
        paymentDetailsLiveData.setValue(paymentDetails);
    }

    public String getPaymentTypeName(String paymentTypeId) {
        for (PaymentType paymentType : paymentTypes) {
            if (paymentType.getPaymentTypeId().equalsIgnoreCase(paymentTypeId))
                return paymentType.getPaymentTypeName();
        }
        return "";
    }

    public void addPaymentDetail(PaymentDetail paymentDetail) {
        paymentDetails.add(paymentDetail);
        paymentDetailsLiveData.setValue(paymentDetails);
    }

    public void removePaymentDetail(PaymentDetail paymentDetail) {
        paymentDetails.remove(paymentDetail);
        paymentDetailsLiveData.setValue(paymentDetails);
    }

    public void refreshPaymentTypes() {
        paymentTypesIncluded = new ArrayList<>(paymentTypes);
        if (paymentDetails.size() > 0) {
            for (PaymentDetail paymentDetail : paymentDetails) {
                removePaymentType(paymentDetail.getPaymentTypeId());
            }
        }
    }

    private void removePaymentType(String paymentTypeId) {
        for (int i = 0; i < paymentTypesIncluded.size(); i++) {
            if (paymentTypesIncluded.get(i).getPaymentTypeId().equalsIgnoreCase(paymentTypeId)) {
                paymentTypesIncluded.remove(i);
                return;
            }
        }
    }
}
