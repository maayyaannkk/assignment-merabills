package in.merabills.myapplication;

import android.app.Dialog;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import in.merabills.myapplication.custom.DecimalDigitsInputFilter;
import in.merabills.myapplication.custom.TextInputSpinner;
import in.merabills.myapplication.data.PaymentDetail;
import in.merabills.myapplication.data.PaymentType;
import in.merabills.myapplication.util.Utility;
import in.merabills.myapplication.viewModel.MainViewModel;

public class MainActivity extends AppCompatActivity {
    private MainViewModel mainViewModel;
    private ChipGroup chipGroup;
    private MaterialButton buttonAddPayment, buttonSavePayment;
    private TextView textViewTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chipGroup = findViewById(R.id.chipGroup);
        buttonAddPayment = findViewById(R.id.buttonAddPayment);
        buttonSavePayment = findViewById(R.id.buttonSavePayment);
        textViewTotal = findViewById(R.id.textViewTotal);

        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        initViews();
        initListener();
    }

    private void initViews() {
        buttonAddPayment.setOnClickListener(v -> {
            Dialog dialogAddPayment = new Dialog(this);
            dialogAddPayment.setContentView(R.layout.dialog_payment);
            Window window = dialogAddPayment.getWindow();
            if (window != null) {
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            }

            TextInputLayout textInputReference = dialogAddPayment.findViewById(R.id.textInputReference);
            TextInputLayout textInputProvider = dialogAddPayment.findViewById(R.id.textInputProvider);
            TextInputLayout textInputAmount = dialogAddPayment.findViewById(R.id.textInputAmount);
            TextInputSpinner textInputSpinnerPaymentType = dialogAddPayment.findViewById(R.id.textInputSpinnerPaymentType);
            MaterialButton buttonCancel = dialogAddPayment.findViewById(R.id.buttonCancel);
            MaterialButton buttonContinue = dialogAddPayment.findViewById(R.id.buttonContinue);

            textInputAmount.getEditText().setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});
            textInputSpinnerPaymentType.setItemList(new ArrayList<>(mainViewModel.getPaymentTypesIncluded()));
            textInputSpinnerPaymentType.setOnItemSelectedListener(spinnerItem -> {
                if (((PaymentType) spinnerItem).isNeedsExtra()) {
                    textInputProvider.setVisibility(View.VISIBLE);
                    textInputReference.setVisibility(View.VISIBLE);
                } else {
                    textInputProvider.setVisibility(View.GONE);
                    textInputReference.setVisibility(View.GONE);
                }
            });

            buttonContinue.setOnClickListener(view -> {
                if (!TextUtils.isEmpty(textInputAmount.getEditText().getText().toString()) && textInputSpinnerPaymentType.getSelectedItem() != null) {

                    try {
                        Double.parseDouble(textInputAmount.getEditText().getText().toString());
                    } catch (NumberFormatException e) {
                        Toast.makeText(this, "Invalid Amount", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    PaymentDetail paymentDetail = new PaymentDetail();
                    paymentDetail.setPaymentAmount(Double.parseDouble(textInputAmount.getEditText().getText().toString()));
                    paymentDetail.setPaymentTypeId(textInputSpinnerPaymentType.getSelectedItem().getItemId());
                    paymentDetail.setProvider(textInputProvider.getVisibility() == View.VISIBLE ? textInputProvider.getEditText().getText().toString().trim() : null);
                    paymentDetail.setReference(textInputReference.getVisibility() == View.VISIBLE ? textInputReference.getEditText().getText().toString().trim() : null);

                    mainViewModel.addPaymentDetail(paymentDetail);

                    dialogAddPayment.dismiss();
                } else {
                    Toast.makeText(this, "Please fill all details to continue", Toast.LENGTH_SHORT).show();
                }
            });

            buttonCancel.setOnClickListener(view -> dialogAddPayment.dismiss());
            dialogAddPayment.show();
        });
        buttonSavePayment.setOnClickListener(v -> {
            Utility.writePaymentsToFile(this, mainViewModel.getPaymentDetails());
            finish();
        });
    }

    private void initListener() {
        mainViewModel.getPaymentDetailsLiveData().observe(this, paymentDetails -> {
            chipGroup.removeAllViews();
            if (paymentDetails != null && paymentDetails.size() > 0) {
                double total = 0.0;
                for (PaymentDetail paymentDetail : paymentDetails) {
                    Chip chip = (Chip) LayoutInflater.from(this).inflate(R.layout.chip_item_payment, chipGroup, false);
                    chip.setText(getString(R.string.label_payment, mainViewModel.getPaymentTypeName(paymentDetail.getPaymentTypeId()), paymentDetail.getPaymentAmount()));
                    chip.setOnCloseIconClickListener(v -> {
                        mainViewModel.removePaymentDetail(paymentDetail);
                    });
                    chipGroup.addView(chip);
                    total += paymentDetail.getPaymentAmount();
                }
                textViewTotal.setText(getString(R.string.label_total_payment, total));
            } else {
                textViewTotal.setText(getString(R.string.label_total_payment, 0.0));
            }
            mainViewModel.refreshPaymentTypes();
        });
    }

}