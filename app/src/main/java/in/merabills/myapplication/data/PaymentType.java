package in.merabills.myapplication.data;

import in.merabills.myapplication.custom.SpinnerItem;

public class PaymentType implements SpinnerItem {
    private String paymentTypeId;
    private String paymentTypeName;
    boolean needsExtra = false;

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public boolean isNeedsExtra() {
        return needsExtra;
    }

    @Override
    public String getItemId() {
        return getPaymentTypeId();
    }

    @Override
    public String getItemName() {
        return getPaymentTypeName();
    }

    @Override
    public String toString() {
        return getPaymentTypeName();
    }
}
