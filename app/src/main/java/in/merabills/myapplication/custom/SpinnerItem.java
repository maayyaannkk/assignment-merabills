package in.merabills.myapplication.custom;

public interface SpinnerItem {
    String getItemId();

    String getItemName();
}
