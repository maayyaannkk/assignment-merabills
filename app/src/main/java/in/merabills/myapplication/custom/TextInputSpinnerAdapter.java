package in.merabills.myapplication.custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import in.merabills.myapplication.R;

public class TextInputSpinnerAdapter extends ArrayAdapter<SpinnerItem> {

    public TextInputSpinnerAdapter(@NonNull Context context, int resource, @NonNull List<SpinnerItem> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView,  ViewGroup parent) {
        return getRowView(position);
    }

    @Override
    public View getDropDownView(int position, View convertView,  ViewGroup parent) {
        return getRowView(position);
    }

    private View getRowView(int position) {
        SpinnerItem rowItem = getItem(position);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = null;
        if (inflater != null && rowItem != null) {
            rowView = inflater.inflate(R.layout.list_item_custom_spinner, null, false);

            TextView txtTitle = rowView.findViewById(R.id.textView);
            rowView.setTag(rowItem);

            txtTitle.setText(rowItem.getItemName());
        }
        return rowView;
    }
}
