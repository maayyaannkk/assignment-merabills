package in.merabills.myapplication.custom;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import in.merabills.myapplication.R;

public class TextInputSpinner extends TextInputLayout {

    private AppCompatAutoCompleteTextView autoCompleteTextView;
    private SpinnerItem selectedItem;
    private OnItemSelectedListener onItemSelectedListener;

    public TextInputSpinner(@NonNull Context context) {
        this(context, null);
    }

    public TextInputSpinner(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TextInputSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    public void setItemList(List<SpinnerItem> itemList) {
        autoCompleteTextView.setTag(itemList);
        autoCompleteTextView.setAdapter(new TextInputSpinnerAdapter(getContext(), R.layout.list_item_custom_spinner, itemList));
    }

    public SpinnerItem getSelectedItem() {
        return selectedItem;
    }

    private void init() {
        autoCompleteTextView = new AppCompatAutoCompleteTextView(getContext());
        autoCompleteTextView.setKeyListener(null);
        autoCompleteTextView.setSingleLine(true);
        autoCompleteTextView.setTextSize(15);
        autoCompleteTextView.setFocusable(false);
        autoCompleteTextView.setCursorVisible(false);
        autoCompleteTextView.setOnItemClickListener((parent, view, position, id) -> {
            if (getError() != null) {
                setError(null);
            }
            this.selectedItem = (SpinnerItem) view.getTag();
            if (onItemSelectedListener != null)
                onItemSelectedListener.onItemSelected(this.selectedItem);
        });
        autoCompleteTextView.setEllipsize(TextUtils.TruncateAt.END);
        autoCompleteTextView.setPadding(15, 0, 0, 0);
        addView(autoCompleteTextView);
    }

    public interface OnItemSelectedListener {
        void onItemSelected(SpinnerItem spinnerItem);
    }

}
