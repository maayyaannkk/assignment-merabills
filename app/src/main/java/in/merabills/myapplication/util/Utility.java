package in.merabills.myapplication.util;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

import in.merabills.myapplication.R;
import in.merabills.myapplication.data.PaymentDetail;
import in.merabills.myapplication.data.PaymentType;

public class Utility {

    public static ArrayList<PaymentType> getPaymentTypes(Context context) {
        String paymentTypesJson = inputStreamToString(context.getResources().openRawResource(R.raw.payment_types));
        return new Gson().fromJson(paymentTypesJson, new TypeToken<ArrayList<PaymentType>>() {
        }.getType());
    }

    public static ArrayList<PaymentDetail> getPaymentDetails(Context context) {
        String paymentTypesJson = readPaymentsFile(context);
        return new Gson().fromJson(paymentTypesJson, new TypeToken<ArrayList<PaymentDetail>>() {
        }.getType());
    }

    public static void writePaymentsToFile(Context context, ArrayList<PaymentDetail> paymentDetails) {
        String body = new Gson().toJson(paymentDetails);
        File dir = new File(context.getFilesDir(), "merabills");
        if (!dir.exists()) {
            dir.mkdir();
        }

        try {
            File file = new File(dir, "LastPayment.txt");
            FileWriter writer = new FileWriter(file);
            writer.append(body);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String readPaymentsFile(Context context) {
        File dir = new File(context.getFilesDir(), "merabills");
        if (!dir.exists()) {
            dir.mkdir();
        }
        try {
            File file = new File(dir, "LastPayment.txt");
            if (!file.exists()) {
                writePaymentsToFile(context, new ArrayList<>());
            }
            InputStreamReader isr = new InputStreamReader(new FileInputStream(file));
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static String inputStreamToString(InputStream inputStream) {
        return new Scanner(inputStream).useDelimiter("\\A").next();
    }
}
